Ученые любят искать первое упоминание своей науки. К примеру, я видел статью, где всерьез утверждалось, что первые опыты по [электрической стимуляции мозга](https://en.wikipedia.org/wiki/Transcranial_direct-current_stimulation) были проведены в Древнем Риме, когда кого-то ударил током электрический угорь. Так или иначе, обычно, историю электрофизиологии принято отсчитывать примерно от опытов Луиджи Гальвани (XVIII век). В этом цикле статей мы попробуем рассказать небольшую часть того, что наука, узнала за последние 300 лет  про электрическую активность мозга человека, про то, какие профиты из всего этого можно извлечь.
![](https://habrastorage.org/webt/3n/zv/b2/3nzvb2sg_vo1eu3gfoyonbdx6i0.png)
<cut/>
### Откуда берется электрическая активность мозга

Мозг состоит из нейронов и глии. Нейроны проявляют электрическую активность, глия тоже может это делать, но по-другому [[1](https://sci-hub.se/10.1002/glia.23525)], [[2](https://sci-hub.se/10.1126/science.298.5593.556)], и мы на нее сегодня обращать внимания не будем.
Электрическая активность нейронов заключается в перекачивании между клеткой и окружающей средой ионов натрия, калия и хлора. Между нейронами сигналы передаются с помощью химических медиаторов. Когда медиатор, выделяемый одним нейроном, попадает на подходящий рецептор другого нейрона, он может открыть химически активируемые ионные каналы, и впустить в клетку небольшое количество ионов. В результате клетка немного меняет свой заряд. Если в клетку вошло достаточно много ионов (например, сигнал пришел одновременно на несколько синапсов), открываются другие ионные каналы, зависимые от напряжения (их больше), и клетка за считанные миллисекунды активируется целиком по принципу “все или ничего”, после чего возвращается в прежнее состояние. Этот процесс называется потенциалом действия.
![image](https://habrastorage.org/webt/rt/1l/90/rt1l90ezh_mfqjbind3mcoeydtu.png)



###Как ее можно зарегистрировать

Лучший способ записать активность отдельных клеток - воткнуть в кору электрод. Это может быть один [провод](http://www.neurosurgery.pitt.edu/centers-excellence/clinical-neurophysiology/micro-electrode-recording), может быть [матрица с несколькими десятками каналов](https://www.blackrockmicro.com/electrode-types/utah-array/), может быть [штырь с несколькими сотнями](https://www.neuropixels.org/), а может быть гибкая плата с несколькими тысячами (как тебе такое, [илон маск](https://www.neuralink.com/) ).

На животных это делают уже давно. Иногда по жизненным показаниям (эпилепсия, болезнь Паркинсона, полный паралич) делают на человеке. Пациенты с имплантами способны печатать текст силой мысли, управлять экзоскелетами, и даже контролировать все степени свободы промышленного манипулятора.

<video>http://www.youtube.com/watch?v=QRt8QCx3BCo</video>

Выглядит впечатляюще, но в ближайшее время в каждую районную поликлинику, и, тем более, к здоровым людям, такие методы не придут. Во-первых, это очень дорого — стоимость процедуры для каждого пациента измеряется сотнями тысяч долларов. Во-вторых, имплантация электродов в кору  - все-таки серьезная нейрохирургическая операция со всеми возможными осложнениями и поражением нервной ткани вокруг импланта. В-третьих, сама технология несовершенна — непонятно, что делать с тканевой совместимостью имплантов, и как предотвратить их обрастание глией, в результате чего нужный сигнал со временем перестает регистрироваться. Кроме того, обучение каждого пациента использованию импланта может занимать больше года ежедневных тренировок.

Можно не втыкать провода глубоко в кору, а аккуратно положить на нее — получится электрокортикограмма. Тут сигнал отдельных нейронов уже не зарегистрировать, но можно увидеть активность очень маленьких областей (общее правило -  чем дальше от нейронов, тем хуже пространственное разрешение метода). Уровень инвазивности ниже, но все равно нужно вскрывать череп, поэтому этот метод используется в основном для контроля во время операций.

Можно положить провода даже не на кору, а на твердую мозговую оболочку (тонкий череп, который находится между мозгом и настоящим черепом). Тут уровень инвазивности и возможных осложнений еще ниже, но сигнал все еще довольно качественный. Получится эпидуральная ЭЭГ. Всем хорош метод, однако, тут все равно нужна операция.

Наконец, минимально инвазивный метод исследования электрической активности мозга — электроэнцефалограмма, а именно, запись с помощью электродов, которые находятся на поверхности головы. Метод самый массовый, сравнительно дешевый (топовые приборы стоят не дороже нескольких десятков тысяч долларов, а большинство в разы дешевле, расходники практически бесплатны), и имеет самое высокое временное разрешение из неинвазивных методов - можно изучать процессы восприятия, которые занимают считанные миллисекунды. Недостатки — низкое пространственное разрешение и шумный сигнал, который, однако, содержит достаточное количество информации для некоторых медицинских и нейроинтерфейсных целей.

На картинке с потенциалом действия видно, что у кривой есть две основных части - собственно, потенциал действия (большой пик)  и синаптический потенциал (маленькое изменение амплитуды перед большим пиком). Логично было бы предположить, что то, что мы регистрируем на поверхности головы, является суммой потенциалов действия отдельных нейронов. Однако, на деле все работает наоборот — потенциал действия занимает около 1 миллисекунды и, несмотря на высокую амплитуду, через череп и мягкие ткани не проходит, а вот синаптические потенциалы за счет большей длительности, хорошо суммируются и регистрируются на поверхности черепа. Это было доказано с помощью одновременной записи инвазивными и неинвазивными методами. Также важно, что активность не каждого нейрона может быть зарегистрирована с помощью ЭЭГ (подробнее [тут](https://sci-hub.se/https://journals.sagepub.com/doi/full/10.1177/155005940904000305)).

Важно, что в мозге находится около 86 миллиардов нервных клеток (о том, как это можно с такой точностью посчитать, читайте [тут](https://sci-hub.se/https://onlinelibrary.wiley.com/doi/pdf/10.1002/cne.21974)), и активность одного нейрона в таком шуме считать невозможно. Однако, какую-то информацию все равно вытащить можно. Представьте себе, что вы стоите в центре футбольного стадиона. Пока фанаты просто шумят и разговаривают между собой, вы слышите равномерный гул, но как только даже небольшая часть присутствующих начинает скандировать кричалку, ее уже можно довольно отчетливо расслышать. Точно так же и с нейронами — на поверхности черепа можно увидеть осмысленный сигнал, только если сразу большое количество нейронов проявляют синхронную активность. Для неинвазивной ЭЭГ это примерно 50 тысяч синхронно работающих нейронов.

Впервые идея померить напряжение на голове человека была реализована в 1924 году довольно[ интересной личностью](https://med-history.livejournal.com/80398.html). Первая запись ЭЭГ выглядела вот так:

![](https://habrastorage.org/webt/mt/mk/vs/mtmkvsdvle-6a50bcrel2_w-v-i.png)


Сложно понять, что означает этот сигнал, но сразу видно, что он не похож на белый шум - в нем заметны веретена колебаний высокой амплитуды и отличающейся частоты. Это альфа-ритм - самый заметный ритм мозга, который можно увидеть невооруженным взглядом.

Сейчас, конечно, ритмы ЭЭГ анализируются не на глаз, а математическими методами, среди которых самые простые из которых - спектральные.

![](https://habrastorage.org/webt/9i/ap/58/9iap58isfrsfxspns5m1dju8xts.jpeg)
Разбитый на полосы спектр Фурье электрэнцефалограммы ([источник](https://doi.org/10.3389/fnhum.2013.00056))

Всего есть несколько полос, в которых обычно анализируют ритмическую активность ЭЭГ, вот самые популярные:

**8-14 Гц - **Альфа-ритм. Представлен в основном в затылочных зонах. Сильно увеличивается при закрытии глаз, также подавляется при умственном напряжении и увеличивается при расслаблении. Этот ритм производится, когда возбуждение циркулирует между корой и таламусом. Таламус - своего рода маршрутизатор, который решает, как перенаправлять в кору потоки входящей информации. Когда человек закрывает глаза, ему становится нечего делать, он начинает генерировать фоновую активность, которая и вызывает альфа-ритм в коре. Кроме того, важную роль играет default mode network - сеть структур, которые активны во время спокойного бодрствования, но это уже тема для отдельной статьи.
![](https://habrastorage.org/webt/7i/1b/wj/7i1bwjcbgqsp_9eaup3u0mqxwyw.png)

Разновидность альфа-ритма, с которой его легко перепутать - мю-ритм. Он имеет схожие характеристики, но регистрируется в центральных областях головы, где находится моторная кора. Важная особенность - его мощность уменьшается, когда человек двигает конечностями, или даже думает о том, чтобы это сделать.

**14-30 Гц - **Бета-ритм. Больше выражен в лобных долях мозга. Увеличивается при умственном напряжении.

**30+ Гц - **Гамма-ритм. Может быть, где-то внутри мозга он и есть, но большая часть того, что можно записать с поверхности, происходит от мышц. Выяснили это [следующим образом](https://sci-hub.se/10.1016/j.clinph.2007.04.027):

Нужно каким-то образом убрать мышечную активность с головы, чтобы записать ЭЭГ с мышцами и без. К сожалению, нет простого способа отключить мышцы на голове, не отключив их во всем теле. Берем ученого (никто другой на такое бы не согласился), накачиваем его миорелаксантом, в результате чего у него отключаются все мышцы. Проблема - если отключить все мышцы, в том числе диафрагму и межреберные, то он не сможет дышать. Решение - кладем его на ИВЛ. Проблема - он еще и говорить без мышц не может. Решение - наложим ему на руку жгут, чтобы туда не попадал миорелаксант, тогда он может этой рукой подавать сигналы. Проблема - если затянуть эксперимент, то рука отвалится. Решение - прекращаем эксперимент когда ученый перестает чувствовать руку, и надеемся, что все закончится хорошо. Результат - доля в спектре частот ЭЭГ больше 20 Гц на фоне миорелаксанта становится меньше в 10-200 раз, чем выше частота, тем выше падение.

**1-4 Гц - **Дельта-ритм. Выражен во время фазы, внезапно, дельта-сна (самый глубокий сон), также повышается при стрессе.

Кроме ритмической активности, в ЭЭГ есть еще вызванная. Если мы точно знаем, в какой момент мы показываем человеку стимул (это может быть картинка, звук, тактильное ощущение и даже [запах](https://sci-hub.se/10.1177/000348949310200102)), мы можем посмотреть, какая была реакция именно на этот стимул. Соотношение сигнал-шум такого ответа по отношению к фоновой ЭЭГ довольно низкое, но если мы покажем стимул, к примеру, 10 раз, нарежем ЭЭГ относительно момента предъявления и усредним, то можно получить довольно подробные кривые, которые называют вызванными потенциалами (не путать с потенциалами действия).

![](https://habrastorage.org/webt/kf/ph/yx/kfphyx65y3p76ym6bgplkvvhb9i.png)


Это вызванный потенциал на звук. Подробности оставим психофизиологам - тут нам достаточно понимать, что каждый экстремум что-то да означает. При достаточном усреднении будут видны ответы структур начиная от слухового нерва (I) и заканчивая ассоциативной корой (P2).

###Что с ней можно сделать 

Сделать можно много чего, но сегодня мы сконцентрируемся на нейрокомпьютерных интерфейсах. Это системы анализа ЭЭГ в реальном времени, которые позволяют отдавать компьютеру или роботу команды без помощи мышц — самое близкое к телекинезу, что может предоставить современная наука.

Самое очевидное, что приходит в голову — сделать интерфейс на ритмической активности. Мы же помним, что альфа-ритма мало, когда человек напряжен, и много, когда он расслаблен? Вот и расслабляйтесь. Пишем ЭЭГ, делаем преобразование Фурье, когда мощность в окне вокруг 10 герц стала выше определенного порога, зажигаем лампочку — вот и контроль компьютера силой мысли. Тот же принцип может позволить управлять другими ритмами. За счет простоты и нетребовательности к оборудованию появилось достаточно много игрушек, работающих на этом принципе - [Neurosky](http://neurosky.com/), [Emotiv](https://www.emotiv.com/), тысячи их. В принципе, если хорошо постараться, человек может научиться приходить в нужное состояние, которое будет правильно классифицироваться. Проблема потребительских девайсов в том, что они часто пишут не очень качественный сигнал, и поголовно не умеют вычитать артефакты от движения глаз и мимических мышц. В результате появляется реальная возможность научиться управлять мышцами и глазами, а не мозгом (а подсознание работает так, что чем больше стараться этого не делать, тем хуже будет получаться). Кроме того, само соотношение сигнал-шум в ритмах довольно низкое, и интерфейс работает медленно и неточно (если получится правильно угадать состояние с точностью больше 70% - это уже достижение). Да и научная база по состояниям кроме расслабления и концентрации, мягко говоря, зыбкая. Тем не менее, при правильной реализации метод может иметь свое применение.

Важный подвид интерфейсов на ритмах — представление движений. Тут человеку предлагается не воображать что-то абстрактно расслабляющее, а представлять движение, скажем, правой руки. Если делать это правильно (а научиться правильному представлению сложно), можно выявить снижение мю-ритма в левом полушарии. Точность таких интерфейсов тоже крутится вокруг 70%, но они находят свое применение в тренажерах для [восстановления после инсультов и травм](https://npo-at.com/production/ekzokist-2-3)**,** в том числе при помощи различных экзоскелетов, поэтому они все равно нужны.

Другой класс ЭЭГ-нейроинтерфейсов основывается на использовании вызванной активности всех сортов. Эти интерфейсы отличаются очень высокой надежностью, при удачном стечении обстоятельств приближающейся к 100%.

Самый популярный вид нейроинтерфейсов включает в себя потенциал Р300. Он возникает тогда, когда человек пытается выделить один нужный ему стимул среди многих ненужных. 
![](https://habrastorage.org/webt/hh/lx/o2/hhlxo2ajxum2-r_p_aselbotcoe.gif)
![](https://habrastorage.org/webt/co/rt/tc/corttc4jqr9edcp_gvmjuc-n-ms.png)

К примеру, если вот тут пытаться посчитать, сколько раз загорится буква “А”, и при этом не обращать внимания на все остальные, то в ответ на этот стимул при усреднении мы увидим красную линию, а при усреднении всех остальных - синюю. Разница между ними заметна невооруженным взглядом, и обучить классификатор, который будет их различать, не составляет особого труда.

Такие интерфейсы обычно не очень красивые, и не очень быстрые (печать одной буквы займет около 10 секунд), но могут быть полезны полностью парализованным пациентам.


Кроме того, в ИМК-Р300 есть когнитивный компонент - мало просто смотреть на букву, надо активно обращать на нее внимание. Это позволяет, при выполнении определенных условий, делать на этой технологии довольно интересные игры (но это тема для другой статьи).

За счет того, что Р300 это когнитивный потенциал, для него не очень важно, что, собственно, показывают человеку, главное, чтобы он мог на это реагировать. В результате интерфейс будет работать, даже если буквы будут сменять друг друга в одной точке - это полезно для пациентов, которые не могут двигать глазами.
![](https://habrastorage.org/webt/wk/ib/n4/wkibn4o4znr4hbjunqolblqh-eg.gif)

Есть и другие интересные вызванные потенциалы, в частности SSVEP (ЗВПУС)  - потенциалы стабильного состояния. Если искать аналогии в области связи, то Р300 работает как рация - сигналы от разных стимулов разделяются по времени, а SSVEP это классический FDMA - разделение по несущей частоте, как в  GSM-связи.
<spoiler title="осторожно, эпилептические мигалки">
<video>https://youtu.be/ZupEt1uvcls?t=89</video>
</spoiler>

Нужно показать человеку несколько стимулов, которые мигают с разными частотами. При выборе стимула, на него достаточно внимательно посмотреть, и через несколько секунд его частота магическим образом окажется в зрительной коре, откуда ее можно вытащить корреляционными или спектральными методами. Это быстрее и проще, чем считать буквы для Р300, но долго смотреть на такое мигание тяжело.

Там, где есть FDMA, там самое место CDMA:
<spoiler title="осторожно, еще более эпилептические мигалки">
<video>https://youtu.be/te00PxGqwpM?t=38</video>
</spoiler>

![](https://habrastorage.org/webt/ly/vv/zg/lyvvzgceplykjn3qohewgattvfk.png)
Серое - бинарная последовательнсть, цветное - вызванная ей активность во всех каналах, карта - распределение выраженности потенциала в ЭЭГ. Видно, что максимум на затылке - в зрительных областях

Можно модулировать мигание стимулов не частотами и фазами, а [ортогогнальными бинарными последовательностями](https://vestnikrgmu.ru/files/issues/2019/2/2019-2-4_ru.pdf?lang=ru), которые точно так же окажутся в зрительной коре и отклассифицируются с помощью корреляционного анализа. Это может помочь немного оптимизировать обучение классификатора и ускорить работу интерфейса - на одну букву может уходить меньше 2 секунд. За счет удачного выбора цветов можно сделать интерфейс чуть менее вырвиглазным, хотя полностью от мигания избавиться не получится. К сожалению, когнитивный компонент тут не так сильно выражен - отслеживание движений глаз дает сопоставимые результаты, но технически проще, дешевле и удобнее.

Когда я говорю о том, насколько хорошо могут работать те или иные типы интерфейсов, приходится постоянно оперировать соотношением сигнал-шум. Действительно, вызванные потенциалы имеют низкую амплитуду - около 5 микровольт, при том, что фоновый альфа-ритм запросто может иметь амплитуду в 20. Такой слабый сигнал кажется довольно сложным для классификации, но на самом деле все это довольно просто, если правильно поставить эксперимент и хорошо записать ЭЭГ. Сейчас большинство академических исследований сосредоточено в области придумывания новых классификаторов, в том числе применения нейросетей, но довольно хорошего уровеня можно достигнуть уже с самыми простыми линейными классификаторами из scikit-learn. К примеру, хороший датасет с Р300 и кодом лежит [здесь](https://www.kaggle.com/c/inria-bci-challenge/discussion).

Нейрокомпьютерные интерфейсы - развивающаяся технология, выглядит как магия, особенно для неподготовленного человека. Однако в реальности это метод, в котором много неочевидных сложностей. Секрет здесь, как и с любой технологией, заключается в том, чтобы учитывать все ограничения и находить такие сферы ее применения, в которых эти ограничения не мешают работе. 
